       IDENTIFICATION DIVISION. 
       PROGRAM-ID. TRADER9.
       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT INPUT1-FILE ASSIGN TO "trader9.dat" 
           ORGANIZATION IS LINE SEQUENTIAL.
       DATA DIVISION. 
       FILE SECTION. 
       FD  INPUT1-FILE.
       01  INPUT-BUFFER.
           88 END-OF-INPUT1-FILE VALUE HIGH-VALUE.
           05 COL-A  PIC X(2).
           05 COL-B  PIC X(2).
           05 COL-COUNT  PIC 9(4).
       
       WORKING-STORAGE SECTION. 
       01  MAX PIC 9(4).
       01  COL-A-MAX PIC 9(4).
       01  COL-A-PROCESS PIC X(5).
       01  SUM-INCOME PIC 9(4).
       01  COL-B-SUM PIC 9(4).
       01  COL-B-PROCESS PIC X(5).
       01  PROVINCE  PIC X(2).
       01  P-INCOME  PIC 9(4).
       01  MEMBER  PIC 9(2).
       01  MEMBER-INCOME PIC 9(6).
       01  RPT-HEADER.
           05 FILLER PIC X(10) VALUE "PROVINCE".
           05 FILLER PIC X(10) VALUE "P INCOME".
           05 FILLER PIC X(10) VALUE "MEMBER".
           05 FILLER PIC X(15) VALUE "MEMBER INCOME".
       01  RPT-ROW.
           05 RPT-COL-A PIC BBX(2).
           05 FILLER PIC X(5) VALUE SPACES .
      *    05 RPT-COL-B PIC BBX(2).
      *    05 FILLER PIC X(5) VALUE SPACES .
           05 RPT-COL-MAX PIC ZZZ9.
       01  RPT-FOTTER.
           05 FILLER PIC X(15) VALUE "MAX PROVIBCE: ".
           05 RPT-MAX PIC 9(2).
           05 FILLER PIC X(20) VALUE "   SUM INCOME: ".
           05 RPT-SUM PIC X(20).
           

       PROCEDURE DIVISION .
       BEGIN.
           OPEN INPUT INPUT1-FILE 
           DISPLAY RPT-HEADER 
           PERFORM READ-LINE
           PERFORM PROCESS-COL-A UNTIL END-OF-INPUT1-FILE
      *    PERFORM PROCESS-COL-B UNTIL END-OF-INPUT1-FILE
           MOVE MAX TO RPT-MAX 
           DISPLAY RPT-FOTTER 
           CLOSE INPUT1-FILE 

           GOBACK
           .

       PROCESS-COL-A.
           MOVE COL-A TO COL-A-PROCESS
           MOVE COL-A-PROCESS TO RPT-COL-A
           MOVE ZERO TO COL-A-MAX
           PERFORM PROCESS-LINE UNTIL COL-A NOT = COL-A-PROCESS .
           MOVE COL-A-PROCESS TO RPT-COL-A.
           MOVE COL-A-MAX TO RPT-COL-MAX .
           DISPLAY RPT-ROW .
       
      *PROCESS-COL-B.
      *    MOVE COL-B TO COL-B-PROCESS
      *    MOVE COL-B-PROCESS TO RPT-COL-B
      *    MOVE ZERO TO COL-B-MAX
      *    PERFORM PROCESS-LINE UNTIL COL-B NOT = COL-B-PROCESS .
      *    MOVE COL-B-PROCESS TO RPT-COL-B.
      *    MOVE COL-B-MAX TO RPT-COL-MAX .
      *    DISPLAY RPT-ROW .

       PROCESS-LINE.
           ADD COL-COUNT TO MAX , COL-A-MAX 
           PERFORM READ-LINE.

       READ-LINE.
           READ INPUT1-FILE 
              AT END SET END-OF-INPUT1-FILE TO TRUE
           END-READ.
           
        

       



       


            

    

       